import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dettagliCorso.dart';

class ElencoCorsiNew extends StatefulWidget {
  const ElencoCorsiNew({Key key, @required this.title}) : super(key: key);
  final String title;

  @override
  _ElencoCorsiNewState createState() => _ElencoCorsiNewState();
}

class _ElencoCorsiNewState extends State<ElencoCorsiNew> {
  Stream<QuerySnapshot> _iceCreamStores;

  @override
  void initState() {
    super.initState();
    _iceCreamStores = Firestore.instance
        .collection('corsi')
        // .orderBy('nomeCorso')
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: Color(0xFFBF5347),
      //   title: Text(widget.title),
      // ),
      body: StreamBuilder<QuerySnapshot>(
        stream: _iceCreamStores,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return CircularProgressIndicator();
          }
          if (!snapshot.hasData) {
            return CircularProgressIndicator();
          }

          return Stack(
            fit: StackFit.loose,
            children: <Widget>[
              sfondo(),
              customAppBar(context),
              StoreCarousel(
                documents: snapshot.data.documents,
              ),
            ],
          );
        },
      ),
    );
  }
}

Widget customAppBar(BuildContext cont) {
  return GestureDetector(
      child: Container(
        padding: EdgeInsets.all(15),
        alignment: Alignment.topLeft,
        child:
            //  CircleAvatar(
            //   backgroundColor: Colors.amber,
            //   child:
            Icon(
          Icons.arrow_back,
          size: 30,
          color: Colors.amber,
        ),
        //)
      ),
      onTap: () {
        Navigator.pop(cont);
      });
}

Widget sfondo() {
  return Container(
      decoration: new BoxDecoration(
          color: Colors.transparent,
          boxShadow: [
            BoxShadow(
              color: Colors.transparent,
              blurRadius: 0.0,
            ),
          ],
          image: new DecorationImage(
              colorFilter: ColorFilter.mode(Colors.grey[500], BlendMode.darken),
              fit: BoxFit.fitHeight,
              image: new AssetImage("assets/bg2.jpg")
              // new NetworkImage(
              //     "https://firebasestorage.googleapis.com/v0/b/palestragestionale.appspot.com/o/imgApp%2Fbg2.jpg?alt=media&token=e1e5b88f-f2be-48b2-82fe-fe095bd66b57")
              )));
}

class StoreCarousel extends StatelessWidget {
  const StoreCarousel({
    Key key,
    @required this.documents,
  }) : super(key: key);
  final List<DocumentSnapshot> documents;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: const EdgeInsets.only(top: 55),
        child: SizedBox(
          child: documents.length == 0
              ? loading(context)
              : ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: documents.length,
                  itemBuilder: (builder, index) {
                    return SizedBox(
                      child: StoreListTile(
                        document: documents[index],
                      ),
                    );
                  },
                ),
        ),
      ),
    );
  }

  Widget loading(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          height: (MediaQuery.of(context).size.height / 2) + 100,
          alignment: Alignment.topCenter,
        ),
        Center(
          child: Container(
            alignment: Alignment.bottomCenter,
            height: MediaQuery.of(context).size.height / 5,
            width: MediaQuery.of(context).size.width / 2,
            child: FlareActor(
              "assets/flare/Loading_Gears.flr",
              alignment: Alignment.topCenter,
              fit: BoxFit.contain,
              animation: "spin1",
              color: Colors.amber,
            ),
          ),
        ),

        // FlareActor(
        //   "assets/flare/loading_rainbow.flr",
        //   alignment: Alignment.topCenter,
        //   fit: BoxFit.none,
        //   animation: "Untitled",
        //   color: Colors.blue,
        // ),
        // FlareActor("assets/flare/loading_circle_three.flr", // NO
        //     alignment: Alignment.topCenter,
        //     fit: BoxFit.contain,
        //     animation: "loading_circle"),
        // FlareActor("assets/flare/Loading_circular.flr",
        //     alignment: Alignment.center,
        //     fit: BoxFit.contain,
        //     animation: "Circular_load"),

        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            // Container(
            //   height: 30,
            // ),
            Center(
                child: Text(
              "Caricamento...\nControllare la connessione dati",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontStyle: FontStyle.italic,
                  fontSize: 12),
            )),
          ],
        ),
      ],
    );
  }
}

class StoreListTile extends StatefulWidget {
  const StoreListTile({
    Key key,
    @required this.document,
  }) : super(key: key);

  final DocumentSnapshot document;

  @override
  State<StatefulWidget> createState() {
    return _StoreListTileState();
  }
}

class _StoreListTileState extends State<StoreListTile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget _loadingListTileImage(BuildContext context) {
    return Center(
      child: Container(
        alignment: Alignment.center,
        // height: MediaQuery.of(context).size.height /2,
        // width: MediaQuery.of(context).size.width / 3,
        child: FlareActor(
          "assets/flare/loading_circle_three.flr",
          alignment: Alignment.topCenter,
          fit: BoxFit.contain,
          animation: "loading_circle",
          color: Colors.amber,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Color(int.parse(widget.document['colore'])),
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(5.0),
            topRight: const Radius.circular(5.0),
            bottomRight: const Radius.circular(5.0),
            bottomLeft: const Radius.circular(5.0),
          )),
      margin: EdgeInsets.only(left: 10, right: 10, top: 10),
      child: Center(
          child: GestureDetector(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) =>
                        DettagliCorso(corso: widget.document)));
              },
              child: Row(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height / 9,
                    width: MediaQuery.of(context).size.width / 4,
                    child: ClipRRect(
                      child: CachedNetworkImage(
                        fit: BoxFit.fitHeight,
                        filterQuality: FilterQuality.high,
                        placeholder: (context, url) =>
                            _loadingListTileImage(context),
                        imageUrl: widget.document['immagine'],
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(5)),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width -
                        MediaQuery.of(context).size.width / 3,
                    child: ListTile(
                      title: RichText(
                          text: TextSpan(
                              text: widget.document['nomeCorso'],
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15))),
                      subtitle: RichText(
                          text: TextSpan(
                              text: widget.document['descrizione'],
                              style: TextStyle(fontSize: 12))),
                    ),
                  )
                ],
              ))),
    );
  }
}
