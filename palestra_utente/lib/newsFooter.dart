import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Questo widget rappresenta il footer presente in qualche schermata con le news inviate dal gestore
class NewsFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: Firestore.instance
            .collection('generale')
            .document('messaggioUtenti')
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData &&
              snapshot.connectionState != ConnectionState.done)
            return LinearProgressIndicator(
                backgroundColor: Colors.grey[300],
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue));
          return Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                    color: Color(0xFFBF5347),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10))),
                alignment: Alignment.center,
                child: Text(snapshot.data['messaggioUtenti'],
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    )),
              ));
        });
  }
}
