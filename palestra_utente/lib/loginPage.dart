import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:palestra_utente/areaPersonale.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'delayed_animation.dart';

class LoginPage extends StatefulWidget {
  final bool forceToLoginPage;

  LoginPage({Key key, this.forceToLoginPage}) : super(key: key);

  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  String username = "";
  String password = "";
  bool _logged = false;
  final int delayedAmount = 100;
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final focus = FocusNode();

  bool usernameCorrect = false;
  bool pswdCorrect = false;

  @override
  void initState() {
    if (widget.forceToLoginPage) 
      _setLogged("", false);
    _loadLoginInfo();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _height;

    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('utenti').snapshots(),
      builder: (context, snapshot) {
        _height = MediaQuery.of(context).size.height;
        if (!snapshot.hasData)
          return LinearProgressIndicator(
              backgroundColor: Colors.grey[300],
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue));
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          home: GestureDetector(
            onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
            child: Scaffold(
              key: _scaffoldKey,
              body: Form(
                key: _formKey,
                autovalidate: true,
                child: Stack(children: <Widget>[
                  Container(
                      decoration: new BoxDecoration(
                          color: Colors.transparent,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.transparent,
                              blurRadius: 0.0,
                            ),
                          ],
                          image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: new AssetImage(
                                  "assets/bg1.jpg") //("assets/bgRunning.jpg")
                              // new NetworkImage(
                              //     "https://images.wallpaperscraft.com/image/silhouette_road_running_121163_800x1200.jpg")
                              ))),
                  AnimatedContainer(
                    duration: Duration(milliseconds: 100),
                    height: _height,
                    padding: EdgeInsets.only(top: 25),
                    child: Center(
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Wrap(
                          verticalDirection: VerticalDirection.down,
                          textDirection: TextDirection.ltr,
                          alignment: WrapAlignment.center,
                          crossAxisAlignment: WrapCrossAlignment.start,
                          spacing: 15,
                          children: <Widget>[
                            AvatarGlow(
                              shape: BoxShape.circle,
                              endRadius: 50,
                              duration: Duration(seconds: 2),
                              glowColor: Colors.white24,
                              repeat: true,
                              repeatPauseDuration: Duration(seconds: 2),
                              startDelay: Duration(seconds: 1),
                              child: Material(
                                  elevation: 8.0,
                                  shape: CircleBorder(),
                                  child: CircleAvatar(
                                    backgroundColor: Colors.grey[100],
                                    child: Image.asset(
                                      "assets/logo_gym.png",
                                      fit: BoxFit.cover,
                                    ),
                                    radius: 70.0,
                                  )),
                            ),
                            DelayedAimation(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Center(
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        top: 1, bottom: 1, right: 7, left: 7),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      color: Colors.deepOrange,
                                    ),
                                    child: Text(
                                      "FITNESS STUDIO",
                                      softWrap: true,
                                      style: GoogleFonts.montserrat(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 25),
                                    ),
                                  ),
                                ),
                              ),
                              delay: delayedAmount + 300,
                            ),
                            DelayedAimation(
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: 40, right: 40, top: 40),
                                child: TextFormField(
                                  textInputAction: password == ''
                                      ? TextInputAction.next
                                      : TextInputAction.done,
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.white)),
                                      labelText:
                                          'Codice tessera/Codice fiscale',
                                      labelStyle: GoogleFonts.montserrat(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.white))),
                                  onChanged: (value) {
                                    username = value;
                                  },
                                  onFieldSubmitted: (v) {
                                    if (password == '')
                                      FocusScope.of(context)
                                          .requestFocus(focus);
                                    else {
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                    }
                                  },
                                ),
                              ),
                              delay: delayedAmount + 600,
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            DelayedAimation(
                              child: Container(
                                padding: EdgeInsets.only(left: 40, right: 40),
                                child: TextFormField(
                                  focusNode: focus,
                                  keyboardType: TextInputType.text,
                                  obscureText: true,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.white)),
                                      labelText: 'Password',
                                      labelStyle: GoogleFonts.montserrat(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.white))),
                                  onChanged: (value) {
                                    password = value;
                                  },
                                ),
                              ),
                              delay: delayedAmount + 900,
                            ),
                            SizedBox(
                              height: 30.0,
                            ),
                            DelayedAimation(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Center(
                                  child: RawMaterialButton(
                                    fillColor: Colors.white,
                                    splashColor: Colors.grey[200],
                                    child: Padding(
                                      padding: EdgeInsets.all(10.0),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: const <Widget>[
                                          Icon(
                                            Icons.check_circle_outline,
                                            color: Colors.blue,
                                          ),
                                          SizedBox(
                                            width: 10.0,
                                          ),
                                          Text(
                                            "ACCESSO",
                                            maxLines: 1,
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.blue,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    onPressed: () {
                                      // FIREBASE CONTROLLO AUTENTICAZIONE
                                      if (!_logged) {
                                        for (int i = 0;
                                            i <
                                                    snapshot?.data?.documents
                                                        ?.length ??
                                                0;
                                            i++) {
                                          // id sarebbe codice tessera
                                          if (((snapshot.data.documents[i]
                                                      ["id"] ==
                                                  username) ||
                                              (snapshot.data.documents[i]
                                                      ["codiceFiscale"] ==
                                                  username) ||
                                              (snapshot.data.documents[i]
                                                      ["codiceFiscale"].toLowerCase() ==
                                                  username))) {
                                            usernameCorrect = true;
                                            if (snapshot.data.documents[i]
                                                    ["psw"] ==
                                                password) {
                                              Navigator.pushAndRemoveUntil(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (BuildContext
                                                              context) =>
                                                          AreaPersonale(
                                                              codiceidentificativo:
                                                                  username)),
                                                  (Route<dynamic> route) =>
                                                      false);
                                              _setLogged(username, true);
                                              return;
                                            }
                                          }
                                        }
                                        _scaffoldKey.currentState
                                            .removeCurrentSnackBar();
                                        _scaffoldKey.currentState
                                            .showSnackBar(snackbar());

                                        usernameCorrect = false;
                                      }
                                    },
                                    shape: const StadiumBorder(),
                                  ),
                                ),
                              ),
                              delay: delayedAmount + 1200,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ]),
              ),
              floatingActionButton: DelayedAimation(
                child: Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, right: 20, left: 20),
                  child: Text(
                    "\"Il vero fallimento è rinunciare\" – Zig Ziglar",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.montserrat(
                        color: Colors.white, fontSize: 15),
                  ),
                ),
                delay: delayedAmount + 1500,
              ),
            ),
          ),
        );
      },
    );
  }

  void _loadLoginInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _logged = (prefs.getBool('logged') ?? false);
      username = (prefs.getString(('username') ?? ""));
    });
  }

  _setLogged(String username, bool logged) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('username', username);
      prefs.setBool('logged', logged);
    });
  }

  SnackBar snackbar() {
    return SnackBar(
      elevation: 10,
      duration: Duration(milliseconds: 2500),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20))),
      content: new Text(
        usernameCorrect ? "Password Errata" : "Dati Errati",
        textAlign: TextAlign.center,
        style: GoogleFonts.montserrat(color: Colors.redAccent),
      ),
    );
  }
}
