import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palestra_utente/carroussel.dart';
import 'package:palestra_utente/newsFooter.dart';

class PaginaPrincipale extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
       appBar: new AppBar(
                backgroundColor: Colors.red[600],
                title: new Text('Info'),
              ),
        body: Center(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              width: 900,
              alignment: Alignment.center,
              color:  Colors.blue[600],
              child: Text("FITNESS CLUB PALERMO",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  )),
            ),
          ),
          Expanded(
            flex: 5,
            child: Carroussel()
          ),
          Expanded(
            flex: 2,
            child: Container(
              color: Colors.transparent,
              padding: EdgeInsets.all(10),
              child:
                  Text("ORARI APERTURA\n\n Lun-Ven 9:00/21:00\nSab-Dom 9:00/17:00",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      )),
            ),
          ),
          NewsFooter()
        ],
      ),
    ));
  }
}
