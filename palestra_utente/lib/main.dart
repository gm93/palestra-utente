import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:palestra_utente/areaPersonale.dart';
import 'package:palestra_utente/loginPage.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());
Map<int, Color> color = {
  50: Color.fromRGBO(136, 14, 79, .1),
  100: Color.fromRGBO(136, 14, 79, .2),
  200: Color.fromRGBO(136, 14, 79, .3),
  300: Color.fromRGBO(136, 14, 79, .4),
  400: Color.fromRGBO(136, 14, 79, .5),
  500: Color.fromRGBO(136, 14, 79, .6),
  600: Color.fromRGBO(136, 14, 79, .7),
  700: Color.fromRGBO(136, 14, 79, .8),
  800: Color.fromRGBO(136, 14, 79, .9),
  900: Color.fromRGBO(136, 14, 79, 1),
};

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'FitnessApp',
      theme: ThemeData(
        appBarTheme: new AppBarTheme(elevation: 0),
        primarySwatch: MaterialColor(0xFFBF5347, color),
      ),
      home: MyHomePage(title: 'Fitness App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

// This method is rerun every time setState is called, for instance as done
class _MyHomePageState extends State<MyHomePage> {
  bool _logged = false;
  String username = "";

  void _loadLoginInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _logged = (prefs.getBool('logged') ?? false);
      username = (prefs.getString(('username') ?? ""));
    });
  }

  void initState() {
    super.initState();
    _loadLoginInfo();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return Scaffold(
        body: !_logged
            ? LoginPage(
                forceToLoginPage: false,
              )
            : AreaPersonale(
                codiceidentificativo: username,
              ));
  }
}
