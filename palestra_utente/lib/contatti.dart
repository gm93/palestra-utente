import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palestra_utente/loginPage.dart';

class Contatti extends StatelessWidget {

Widget customAppBar(BuildContext cont) {
  return GestureDetector(
      child: Container(
          padding: EdgeInsets.all(15),
          alignment: Alignment.topLeft,
          child: CircleAvatar(
            backgroundColor: Colors.amber,
            child: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          )),
      onTap: () {
        Navigator.pop(cont);
      });
}

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        // appBar: new AppBar(
        //   backgroundColor: Color(0xFFBF5347),
        //   title: new Text('CONTATTI'),
        // ),
        body: Stack(
          children: <Widget>[
            Container(
                decoration: new BoxDecoration(
                    color: Colors.transparent,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.transparent,
                        blurRadius: 0.0,
                      ),
                    ],
                    image: new DecorationImage(
                      colorFilter: ColorFilter.mode(Colors.grey[500], BlendMode.darken),
                        fit: BoxFit.fill,
                        image:
                            new AssetImage("assets/bg1.jpg"),
                            // new NetworkImage(
                            //     "https://www.t-nation.com/system/publishing/articles/10005529/original/6-Reasons-You-Should-Never-Open-a-Gym.png?1509471214")
                            ))),
                            customAppBar(context),
             Center(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: SelectableText(""),
                    flex: 2,
                  ),
                  Expanded(
                    flex: 1,
                    child: ListTile(
                      leading: Image.asset("assets/location-icon.png"),
                      title: SelectableText("Via città di Palermo, 45",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Expanded(
                    child: Text(""),
                    flex: 1,
                  ),
                  Expanded(
                    flex: 1,
                    child: ListTile(
                      leading: Image.asset("assets/clock-icon.png"),
                      title: SelectableText("LUN-VEN 9:00 / 22:00\nSAB 9:00 / 14:00",
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)
                          ),
                    ),
                  ),
                  Expanded(
                    child: SelectableText(""),
                    flex: 1,
                  ),
                  Expanded(
                    flex: 1,
                    child: ListTile(
                      leading: Image.asset("assets/phone-icon.png"),
                      title: SelectableText("TELEFONO: 0914546772",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Expanded(
                    child: SelectableText(""),
                    flex: 1,
                  ),
                  Expanded(
                    child: ListTile(
                      leading: Image.asset("assets/emailicon.png"),
                      title: SelectableText("EMAIL: fitnessapp@gmail.com",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Expanded(
                    child: SelectableText(""),
                    flex: 1,
                  ),
                  Expanded(
                    flex: 1,
                    child: ListTile(
                      leading: Image.asset("assets/facebookicon.png"),
                      title: SelectableText(
                          "Facebook: Fitness Club Palermo Group (OFFICIAL)",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Expanded(
                    child: SelectableText(""),
                    flex: 1,
                  ),
                  Expanded(
                      flex: 0,
                      child: Container(
                          alignment: Alignment.topCenter,
                          child: Container(
                              padding: EdgeInsets.only(top: 10, bottom: 10),
                              height: 60.0,
                              width: 70.0,
                              child: Material(
                                borderRadius: BorderRadius.circular(20.0),
                                shadowColor: Colors.red,
                                color: Colors.grey[600],
                                //elevation: 7.0,
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                LoginPage(
                                                    forceToLoginPage: true)),
                                        (Route<dynamic> route) => false);
                                    //Navigator.of(context).pop();
                                    // Navigator.removeRouteBelow(context, MaterialPageRoute(
                                    //     builder: (BuildContext context) =>
                                    //         LoginPage(forceToLoginPage: true)));

                                    // Navigator.of(context).push(MaterialPageRoute(
                                    //     builder: (BuildContext context) =>
                                    //         LoginPage(forceToLoginPage: true)));
                                  },
                                  child: Center(
                                    child: Text(
                                      'Logout',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              )))),
                //  NewsFooter()
                ],
              ),
            ),
          ],
        ));
  }
}
