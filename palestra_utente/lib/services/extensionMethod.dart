import 'package:cloud_firestore/cloud_firestore.dart';

extension TimeStampExtension on Timestamp {
  String toShortStringDate() {
    String data = DateTime.fromMillisecondsSinceEpoch(this.seconds * 1000).toString();
    int ora, minuto, giorno, mese, anno;

    ora = DateTime.parse(data).hour;
    minuto = DateTime.parse(data).minute;
    giorno = DateTime.parse(data).day;
    mese = DateTime.parse(data).month;
    anno = DateTime.parse(data).year;

    String returnedData =
        ((ora >= 10 ? ora.toString() : ('0' + ora.toString())) +
            ':' +
            (minuto >= 10 ? minuto.toString() : ('0' + minuto.toString())) +
            ' - ' +
            (giorno >= 10 ? giorno.toString() : ('0' + giorno.toString())) +
            '/' +
            (mese >= 10 ? mese.toString() : ('0' + mese.toString())) +
            '/' +
            (anno.toString()));
    return returnedData;
  }

  String toShortStringDateMinusHour() {
    if (this != null) {
      String data = DateTime.fromMillisecondsSinceEpoch(this.seconds * 1000).toString();

      int giorno, mese, anno;
      giorno = DateTime.parse(data).day;
      mese = DateTime.parse(data).month;
      anno = DateTime.parse(data).year;

      String returnedData =
          ((giorno >= 10 ? giorno.toString() : ('0' + giorno.toString())) +
              '/' +
              (mese >= 10 ? mese.toString() : ('0' + mese.toString())) +
              '/' +
              (anno.toString()));
      return returnedData;
    } else
      return '';
  }
}

extension SpaceWhenUpperCaseExtension on String {
  String spaceWhenCapitalLetter() {
    String str = this;
    String res = '';

    for (int i = 0; i < str.length; ++i) {
      String s = this[i];
      if (s.toUpperCase() == s && !(int.tryParse(s) is int) && !(s == '/')) {
        res += ' ';
        res += s;
      } else {
        res += s;
      }
    }
    return res;
  }

  String trimToUpperCase() {
    String str = this;
    String res = this[0].toUpperCase();
    for (int i = 1; i < str.length; ++i) {
      if (this[i] != ' ') {
        if (this[i - 1] == ' ') {
          res += this[i].toUpperCase();
        } else {
          res += this[i];
        }
      }
    }
    return res;
  }

  
  Timestamp stringToTimestamp() {

      String giorno = this.substring(0,2);
      String mese = this.substring(3,5);
      String anno = this.substring(6,10);
      return Timestamp.fromDate(DateTime(int.parse(anno), int.parse(mese),int.parse(giorno)));

  }


}

// extension DateExtension on DateTime {
// String toShortStringDate() {
//   if (this != null) {
//     String returnedData = '';
//     returnedData = ((this.hour.toString()) +
//         ':' +
//         (this.minute.toString()) +
//         ' - ' +
//         (this.day >= 10 ? this.day.toString() : ('0' + this.day.toString())) +
//         '/' +
//         (this.month >= 10
//             ? this.month.toString()
//             : ('0' + this.month.toString())) +
//         '/' +
//         (this.year.toString().substring(2)));
//     return returnedData;
//   } else
//     return '';
// }
// }

// classic function
// String formattedDate(Timestamp timestamp) {
//   String data =
//       DateTime.fromMillisecondsSinceEpoch(timestamp.seconds * 1000).toString();

//   String returnedData = ((DateTime.parse(data).day >= 10
//           ? DateTime.parse(data).day.toString()
//           : ('0' + DateTime.parse(data).day.toString())) +
//       '/' +
//       (DateTime.parse(data).month >= 10
//           ? DateTime.parse(data).month.toString()
//           : ('0' + DateTime.parse(data).month.toString())) +
//       '/' +
//       (DateTime.parse(data).year.toString()));
//   return returnedData;
// }
