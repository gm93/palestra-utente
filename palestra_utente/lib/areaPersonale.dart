import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palestra_utente/contatti.dart';
import 'package:palestra_utente/newsFooter.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'services/animations.dart';
import 'elencoCorsi.dart';
import 'services/extensionMethod.dart';

class AreaPersonale extends StatefulWidget {
  final String codiceidentificativo;
  AreaPersonale({this.codiceidentificativo}) : super();
  _AreaPersonaleState createState() => _AreaPersonaleState();
}

Future<bool> checkCodice(codice) async {
  var _doc =
      await Firestore.instance.collection('utenti').document(codice).get();
  return _doc.exists;
}

Future<String> correctCodice(codiceidentificativo) async {
  QuerySnapshot _myDoc =
      await Firestore.instance.collection('utenti').getDocuments();
  String result = "";
  if (_myDoc.documents != null) {
    for (var i = 0; i < _myDoc.documents.length; i++) {
      if (_myDoc.documents[i].data["codiceFiscale"].toString() ==
          codiceidentificativo.toString().toUpperCase()) {
        result = _myDoc.documents[i].data["id"].toString();
        break;
      }
    }
  }
  return result;
}

class _AreaPersonaleState extends State<AreaPersonale> {
  FlareActor loadingFlareActor;
  bool displayAnimation = true;
  String codiceidentificativo = '';

  @override
  void initState() {
    loadingFlareActor = new FlareActor("assets/animation/loading2.flr",
        alignment: Alignment.center,
        fit: BoxFit.contain,
        animation: 'preloader',
        color: Colors.blue,
        isPaused: false);

    checkCodice(widget.codiceidentificativo).then((isCodiceTessera) => {
          if (!isCodiceTessera)
            {
              // allora sarà il codice fiscale
              // recupero il codice tessera dal codice fiscale
              correctCodice(widget.codiceidentificativo)
                  .then((codiceTessara) => {
                        if (codiceTessara != '')
                          {
                            setState(() {
                              codiceidentificativo = codiceTessara;
                            })
                          }
                      })
            }
          else
            {
              // aggiorno il valore definitivo di codiceidentificativo per aggiornare l'interfaccia
              setState(() {
                codiceidentificativo = widget.codiceidentificativo;
              })
            }
        });
    super.initState();
  }

  Widget loadingView() {
    return Scaffold(body: Center(child: loadingFlareActor));
  }

  @override
  Widget build(BuildContext context) {
    if (codiceidentificativo == null || codiceidentificativo == '')
      return loadingView();
    return new StreamBuilder(
        stream: Firestore.instance
            .collection('utenti')
            .document(codiceidentificativo)
            .snapshots(),
        builder: (context, snapshot) {
          if (codiceidentificativo == '' ||
              displayAnimation ||
              (snapshot == null ||
                  !snapshot.hasData &&
                      (snapshot.connectionState != ConnectionState.active &&
                          snapshot.connectionState != ConnectionState.done))) {
            Future.delayed(Duration(seconds: 2), () {
              setState(() {
                displayAnimation = false;
              });
            });
            return loadingView();
            // LinearProgressIndicator(
            //     backgroundColor: Colors.grey[300],
            //     valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue));
          }
          final GlobalKey<ScaffoldState> _scaffoldKey =
              new GlobalKey<ScaffoldState>();
          return new Scaffold(
              key: _scaffoldKey,
              drawer: Drawer(
                child: ListView(
                  children: <Widget>[
                    UserAccountsDrawerHeader(
                      accountName: Text("Fitness Studio App"),
                      accountEmail: Text("fitnessApp@gmail.com"),
                      currentAccountPicture: CircleAvatar(
                          backgroundColor: Theme.of(context).platform ==
                                  TargetPlatform
                                      .iOS //fighissimo! Cambia colore in base al dispositivo se iOS o Android
                              ? Colors.green[600]
                              : Colors.white,
                          child: Image.asset(
                            "assets/logo_gym.png",
                            fit: BoxFit.contain,
                          )),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/bg1.jpg"),
                              fit: BoxFit.cover)),
                    ),
                    ListTile(
                      title: Text("Elenco corsi"),
                      leading: Icon(Icons.list),
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => ElencoCorsiNew(
                                  title: "Elenco Corsi",
                                )));
                      },
                    ),
                    ListTile(
                      title: Text("Informazioni"),
                      leading: Icon(Icons.info_outline),
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => Contatti()));
                      },
                    ),
                    //  ListTile(
                    //   title: Text("Modifica i tuoi dati di accesso"),
                    //   leading: Icon(Icons.mode_edit),
                    //   onTap: () {
                    //     Navigator.of(context).pop();
                    //     Navigator.of(context).push(MaterialPageRoute(
                    //         builder: (BuildContext context) => Contatti()));
                    //   },
                    // ),
                  ],
                ),
              ),
              //this will just add the Navigation Drawer Icon
              body: Builder(builder: (BuildContext context) {
                double _height = MediaQuery.of(context).size.height;
                double _width = MediaQuery.of(context).size.width;
                return AnimatedContainer(
                  duration: Duration(seconds: 10),
                  curve: Curves.easeOutExpo,
                  child: Stack(
                    children: <Widget>[
                      Container(
                          decoration: new BoxDecoration(
                              color: Colors.transparent,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.transparent,
                                  blurRadius: 0.0,
                                ),
                              ],
                              image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: AssetImage("assets/bg1.jpg")))),
                      Container(
                          alignment: Alignment.topLeft,
                          height: _height / 12,
                          width: _width,
                          child: Stack(
                            children: <Widget>[
                              IconButton(
                                  padding: EdgeInsets.only(left: 5.0),
                                  onPressed: () =>
                                      Scaffold.of(context).openDrawer(),
                                  icon: Icon(Icons.menu),
                                  iconSize: 30.0,
                                  color: Colors.white),
                              Container(
                                width: _width - _height / 12 + 5,
                                child: Center(
                                  child: Image(
                                    image:
                                        new AssetImage("assets/logo_gym.png"),
                                    // NetworkImage(
                                    //     "http://www.omniafitness.it/wp-content/uploads/2017/07/white_scritta_trasp.png"),
                                    filterQuality: FilterQuality.high,
                                  ),
                                ),
                              ),
                            ],
                          )),
                      Center(
                        child: Column(
                          children: <Widget>[
                            Expanded(
                                flex: 2,
                                child: Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: <Widget>[],
                                )),
                            Expanded(
                              flex: 4,
                              child: Container(
                                height: MediaQuery.of(context).size.height,
                                padding: EdgeInsets.only(bottom: 5),
                                decoration: BoxDecoration(
                                    borderRadius: new BorderRadius.all(
                                        Radius.circular(40))),
                                margin: EdgeInsets.only(
                                    left: 80, right: 80, top: 15, bottom: 25),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    QrImage(
                                      backgroundColor: Colors.white,
                                      data:
                                          codiceidentificativo, //"Siamo i numeri 1",
                                      size: 140.0,
                                      version: 2,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: FadeInWithScale(
                                //  isX: true,
                                duration: 250,
                                // translateXStart: 140.0,
                                //translateXEnd: 0.0,
                                delay: 300,
                                child: Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(10),
                                  child: InkWell(
                                      radius: 40,
                                      child: Card(
                                          elevation: 8,
                                          color: Colors.white,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(20))),
                                          child: ListView(
                                            children: <Widget>[
                                              ListTile(
                                                leading: Icon(
                                                  Icons.person,
                                                  color: Colors.green,
                                                ),
                                                title: Text(snapshot
                                                        .data['nome']
                                                        .toString()
                                                        .toUpperCase() +
                                                    " " +
                                                    snapshot.data['cognome']
                                                        .toString()
                                                        .toUpperCase()),
                                              ),
                                              ListTile(
                                                leading: Icon(Icons.history,
                                                    color: Colors.green),
                                                title: Text(
                                                    "Scad. Certificato Medico: " +
                                                        ((snapshot.data[
                                                                    'certMedico']
                                                                as Timestamp)
                                                            .toShortStringDateMinusHour())),
                                              ),
                                              ListTile(
                                                leading: Icon(
                                                    Icons.format_list_numbered,
                                                    color: Colors.green),
                                                title: Text(
                                                    "Numero ingressi effettuati:  " +
                                                        snapshot.data[
                                                                'numeroIngressi']
                                                            .toString()),
                                              ),
                                              ContrattiAttivi(
                                                  codiceidentificativo:
                                                      codiceidentificativo,
                                                  scaffoldKey: _scaffoldKey),
                                            ],
                                          ))),
                                ),
                              ),
                            ),
                            NewsFooter(),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }));

          // if (displayAnimation) return loadingView();
        });
  }
}

class ContrattiAttivi extends StatefulWidget {
  ContrattiAttivi({Key key, this.codiceidentificativo, this.scaffoldKey})
      : super(key: key);
  String codiceidentificativo;
  dynamic scaffoldKey;
  @override
  _ContrattiAttiviState createState() => _ContrattiAttiviState();
}

class _ContrattiAttiviState extends State<ContrattiAttivi> {
  dynamic contrattiUtente;
  Future<List<DocumentSnapshot>> getContratti(codiceUtente) async {
    List<DocumentSnapshot> result = List<DocumentSnapshot>();
    QuerySnapshot _myDoc =
        await Firestore.instance.collection('contratti').getDocuments();

    if (_myDoc.documents != null) {
      for (var i = 0; i < _myDoc.documents.length; i++) {
        if (_myDoc.documents[i].data["id_utente"].toString() ==
            codiceUtente.toString() && 
            Timestamp.now().compareTo(_myDoc.documents[i].data["scadenzaContratto"]) < 1)
             {
          result.add(_myDoc.documents[i]);
        }
      }
    }
    return result;
  }

  popolamentoContratti(List<DocumentSnapshot> docs) {
    Map<String, String> map = Map<String, String>();
    for (var i = 0; i < docs.length; i++) {
      String nome = docs[i].data['id_abbonamento'].toString();
      map[nome] = (docs[i].data['scadenzaContratto'] as Timestamp)
          .toShortStringDateMinusHour();
    }
    setState(() {
      contrattiUtente = map;
    });
    return map;
  }

  @override
  void initState() {
    getContratti(widget.codiceidentificativo).then((documents) => {
          if (documents.length > 0) {popolamentoContratti(documents)} else {}
        });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        if (contrattiUtente != null)
          return Wrap(
              // spacing: 10,
              crossAxisAlignment: WrapCrossAlignment.start,
              direction: Axis.horizontal,
              alignment: WrapAlignment.spaceEvenly,
              children: [
                ...contrattiUtente.map((nomeContratto, scadenzaContratto) {
                  return MapEntry(
                      nomeContratto,
                      InkWell(
                          onTap: () {
                            widget.scaffoldKey.currentState
                                .removeCurrentSnackBar();
                            widget.scaffoldKey.currentState
                                .showSnackBar(SnackBar(
                              content:
                                  Text('Data scadenza: ' + scadenzaContratto),
                              duration: Duration(seconds: 3),
                            ));
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: Color(0xFFBF5347),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            margin: EdgeInsets.all(10),
                            width: 100,
                            height: 50,
                            child: Center(
                                child: Text(
                              nomeContratto.toString().spaceWhenCapitalLetter(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600),
                            )),
                          )));
                }).values
              ]);
        return ListTile(
          leading: Icon(Icons.warning, color: Colors.red),
          title: Text('Nessun contratto attivo!'),
        );
      },
    );
  }
}
