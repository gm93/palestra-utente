import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DettagliCorso extends StatefulWidget {
  const DettagliCorso({Key key, this.corso}) : super(key: key);
  final dynamic corso;
  @override
  _DettagliCorsoState createState() => _DettagliCorsoState();
}

class _DettagliCorsoState extends State<DettagliCorso> {
  String infoSettimana = "Informati in segreteria";
  @override
  void initState() {
    super.initState();
    getSettimanaByCorso();
  }

  Future<void> getSettimanaByCorso() async {
    QuerySnapshot _myDoc = await Firestore.instance
        .collection('corsi')
        .document(widget.corso['id'])
        .collection('settimana')
        .getDocuments();
    String result = "";
    if (_myDoc.documents != null) {
      for (var i = 0; i < _myDoc.documents.length; i++) {
        result += (i == 0 ? "" : "\n") + (
           (_myDoc.documents[i].data["giorno"] != null ? _myDoc.documents[i].data["giorno"] : "")  +
            " " + (
            (_myDoc.documents[i].data["ora"] != null ? _myDoc.documents[i].data["ora"]: "") +
            " " + (
            (_myDoc.documents[i].data["istruttore"] != null ? _myDoc.documents[i].data["istruttore"]: ""))));
      }
    }
    setState(() {
      infoSettimana = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            child: CachedNetworkImage(
              fadeInDuration: Duration.zero,
              fadeOutDuration: Duration.zero,
              placeholder: (context, url) => loading(),

              // Image.asset(
              //   "assets/" + widget.corso['nomeCorso'] + " i.jpg",
              //   filterQuality: FilterQuality.high,
              //   fit: BoxFit.fitHeight,
              // ),
              imageUrl: widget.corso['immagineInside'],
              filterQuality: FilterQuality.high,
              fit: BoxFit.fitHeight,
            ),
            height: MediaQuery.of(context).size.height, //3
            width: MediaQuery.of(context).size.width,
          ),
          GestureDetector(
              child: Container(
                  padding: EdgeInsets.all(15),
                  alignment: Alignment.topLeft,
                  child: CircleAvatar(
                    backgroundColor: Colors.amber,
                    child: Icon(
                      Icons.arrow_back,
                      size: 30,
                      color: Colors.white,
                    ),
                  )),
              onTap: () {
                Navigator.pop(context);
              }),
          Center(
              child: Container(
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.only(bottom: 0, left: 0),
                  decoration: BoxDecoration(
                      color: Colors
                          .transparent, //Color(int.parse(corso['colore'])),
                      borderRadius: new BorderRadius.all(Radius.circular(30))),
                  margin:
                      EdgeInsets.only(left: 0, right: 0, top: 250, bottom: 0),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Color(int.parse("0xCC000000")),
                          // borderRadius: new BorderRadius.only(
                          // topLeft: const Radius.circular(30.0),
                          // topRight: const Radius.circular(30.0))
                        ),
                        child: ListTile(
                          leading: Icon(Icons.description, color: Colors.white),
                          title: Text(
                            widget.corso["nomeCorso"].toString().toUpperCase(),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          subtitle: Text(
                            widget.corso["descrizione"],
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                        ),
                      ),
                      Container(
                        color: Color(int.parse("0xCC000000")),
                        child: ListTile(
                          leading: Icon(Icons.access_time, color: Colors.white),
                          title: Text("ORARIO - ISTRUTTORE",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                          subtitle: Text(
                            infoSettimana == ''
                                ? "Informati in segreteria"
                                : (infoSettimana), //giorni con orari del corso
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                        ),
                      ),
                      

                    ],
                  )))),
        ],
      ),
    );
  }

  Widget loading() {
    // Container(color: Colors.grey[900], child:FlareActor("assets/flare/Loading_circular.flr", alignment:Alignment.center, fit:BoxFit.contain, animation:"Circular_load"));
    var cont = Container(
      color: Colors.grey[900],
      padding: EdgeInsets.only(right: 50, left: 50, bottom: 50, top: 70),
      child: Stack(
        children: <Widget>[
          Container(
            alignment: Alignment.topCenter,
            // height: MediaQuery.of(context).size.height/3,
            // width: MediaQuery.of(context).size.width/2,
            child: FlareActor(
              "assets/flare/Loading_Gears.flr",
              alignment: Alignment.topCenter,
              fit: BoxFit.scaleDown,
              animation: "spin1",
              color: Colors.amber,
            ),
          ),

          // FlareActor(
          //   "assets/flare/loading_rainbow.flr",
          //   alignment: Alignment.topCenter,
          //   fit: BoxFit.none,
          //   animation: "Untitled",
          //   color: Colors.blue,
          // ),
          // FlareActor("assets/flare/loading_circle_three.flr", // NO
          //     alignment: Alignment.topCenter,
          //     fit: BoxFit.contain,
          //     animation: "loading_circle"),
          // FlareActor("assets/flare/Loading_circular.flr",
          //     alignment: Alignment.center,
          //     fit: BoxFit.contain,
          //     animation: "Circular_load"),

          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                  // height: 10,
                  ),
              Center(
                  child: Text(
                "Caricamento...\nControllare la connessione dati",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontStyle: FontStyle.italic,
                    fontSize: 12),
              )),
            ],
          ),
        ],
      ),
    );
    return cont;
  }
}
